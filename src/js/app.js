import './modernizr';
import 'lazysizes';
import 'lazysizes/plugins/unveilhooks/ls.unveilhooks';
import 'lodash.debounce';
import SweetScroll from 'sweet-scroll';
import { Swiper, EffectFade, Autoplay } from 'swiper';
import Masonry from 'masonry-layout';
import PhotoSwipe from 'photoswipe';
import PhotoSwipeUI_Default  from 'photoswipe/dist/photoswipe-ui-default';
import mapsAPI from './maps';
//import '../js/calendar';

window.addEventListener('DOMContentLoaded', () => {
    document.body.classList.add('loaded');

    const scroller = new SweetScroll({
        offset: -100,
        easing: 'easeInOutSine'
    });

    getScrollPos();
    createImageArray();

    const gMaps = new mapsAPI();
    gMaps.init();
});

window.addEventListener('scroll', () => {
    getScrollPos();
});

//Get scroll position
function getScrollPos() {
    let scrollPos =
        window.scrollY ||
        window.scrollTop ||
        document.getElementsByTagName('html')[0].scrollTop;

    if (scrollPos > 0) {
        document.body.classList.add('scroll');
    } else {
        document.body.classList.remove('scroll');
    }
}

//Scrollspy
window.addEventListener('DOMContentLoaded', () => {
    let observerOptions = {
        rootMargin: '-110px 0px 0px 0px',
        threshold: [0.81]
    };

    const observer = new IntersectionObserver(entries => {
        entries.forEach(entry => {
            const id = entry.target.getAttribute('id');

            if (
                typeof document.querySelector(`nav a[href="#${id}"]`) !=
                    'undefined' &&
                document.querySelector(`nav a[href="#${id}"]`) != null
            ) {
                if (entry.isIntersecting) {
                    document
                        .querySelector(`nav a[href="#${id}"]`)
                        .classList.add('active');
                } else {
                    document
                        .querySelector(`nav a[href="#${id}"]`)
                        .classList.remove('active');
                }
            }
        });
    }, observerOptions);

    // Track all module that have an `id` applied
    document.querySelectorAll('.module[id]').forEach(section => {
        observer.observe(section);
    });
});

//Page Banner
Swiper.use([EffectFade, Autoplay]);
let pageBanner = new Swiper('.page-banner .swiper-container', {
    loop: true,
    speed: 800,
    autoplay: {
        delay: 8000,
    },
    effect: 'fade',
    fadeEffect: {
        crossFade: true
    },
});

//Gallery Masonry
const gallerySection = document.querySelector('.gallery .grid');

let msnry = new Masonry( gallerySection, {
    itemSelector: '.grid-item',
    columnWidth: '.grid-item',
    gutter: 15,
    percentPosition: true
});

//Photoswipe
function getMeta(url){   
    return new Promise((resolve, reject) => {
        let img = new Image();
        img.onload = () => resolve(img);
        img.onerror = () => reject();
        img.src = url;
    });
}

let galleryItems = [];
async function createImageArray() {
    for( const item of gallerySection.querySelectorAll('.grid-item') ) {
        const link = item.querySelector('a');
        let img = await getMeta(link.getAttribute('href'));
    
        let image = {
            src: link.getAttribute('href'),
            w: parseInt(img.width, 10),
            h: parseInt(img.height, 10),
        }
    
        galleryItems.push(image);
    }
}

const galleryItemLink = gallerySection.querySelectorAll('.grid-item a');
const pswpElement =  document.querySelector('.pswp');

for (const link of galleryItemLink) {
    link.addEventListener('click', (e) => {
        e.preventDefault();

        let options = {
            index: parseInt(link.getAttribute('data-index'), 10),
            bgOpacity: 0.7,
            showHideOpacity: false
        };

        let gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, galleryItems, options);
        gallery.init();
    });
}